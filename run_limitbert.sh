#!/usr/bin/env bash
CUDA_VISIBLE_DEVICES=0 python src_srl_syn/run_lm_finetuning.py train \
--save-model-path-base models/limit-bert \
--model-name limit-bert \
--model bert \
--use-electra \
--train-file bert_data/bert_train.txt \
--bert-model bert-large-uncased-whole-word-masking \
--max-seq-length 128 \
--train-batch-size 8 \
--num_train_epochs 2 \
--learning-rate 3e-5 \
--pre_step_tosave 500 \
--pre_step 0 \
--pre_wiki_line 0 \
--p-ptb 0.0 \
--p-constmask 0 \
--p-srlmask 0 \
--p-wordmask 1 \
--p-tokenmask 0